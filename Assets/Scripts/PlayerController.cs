﻿using UnityEngine;
using System.Collections;

public class PlayerController : MovingObject {

    public float maxSpeed = 10f;
    public int direction = 0;
    public bool facingFront = true;
    public bool facingLeft = true;
    public bool moving = false;
    public bool canMove = true;
    public GameObject collisionLevel;

    private GameObject childSprite;

    // Faster parameter hash ids
    int frontHash = Animator.StringToHash("FacingFront");
    int leftHash = Animator.StringToHash("FacingLeft");
    int movingHash = Animator.StringToHash("Moving");
    int jumpHash = Animator.StringToHash("Jump");

    // Used to store a reference to the Player's animator component.
    private Animator animator;

    private GameObject getChildGameObject(GameObject fromGameObject, string withName) {
        foreach (Transform child in fromGameObject.transform)
        {
            // child is your child transform
            if( child.name == withName )
                return child.gameObject;
        }

        return null;
     }

    //Start overrides the Start function of MovingObject
    protected override void Start ()
    {
        // Get a component reference to the Player's animator component
        childSprite = getChildGameObject(gameObject, "Sprite");
        animator = childSprite.GetComponent<Animator>();
        
        // Call the Start function of the MovingObject base class.
        base.Start ();
    }

    private void Update ()
    {
        // Debug.Log("player can move? "+ canMove);
        if( ! canMove )
            return;

        // AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        int horizontal = 0; // Used to store the horizontal move direction.
        int vertical = 0; // Used to store the vertical move direction.

        horizontal = (int) (Input.GetAxisRaw ("Horizontal"));
        vertical = (int) (Input.GetAxisRaw ("Vertical"));
        bool moving = horizontal != 0 || vertical != 0;

        // Set animator parameters
        animator.SetBool("Moving", moving);
        // Change facing direction only if it changes
        if( facingFront == true && vertical > 0 ) {
            animator.SetBool(frontHash, false);
        }
        else if( facingFront == true && vertical < 0 ) {
            animator.SetBool(frontHash, true);
        }

        if( horizontal > 0 ) {
            // facingLeft = false;
            // animator.SetBool(leftHash, false);
            if( facingLeft ) 
                Flip();
        }
        else if( horizontal < 0 ) {
            // facingLeft = true;
            // animator.SetBool(leftHash, true);
            if( ! facingLeft ) 
                Flip();
        }
        
        // Check if we have a non-zero value for horizontal or vertical
        if(horizontal != 0 || vertical != 0)
        {
            // Call AttemptMove passing in the generic parameter Wall, since that is what Player may interact with if they encounter one (by attacking it)
            // Pass in horizontal and vertical as parameters to specify the direction to move Player in.
            Move (horizontal, vertical);
        }

        // int tops = 2;
        string[] tileMaps = new string[] {"allTiles_sheet-128", "platformerTile_06", "land", "buildings"};

        // Get the collision layer
        GameObject collisionLayer = getChildGameObject(collisionLevel, "Collisions");
        if( collisionLayer != null ) {

            // Loop its children (PolygonObjects)
            for(int i = 0; i < collisionLayer.gameObject.transform.GetChildCount(); i++)
            {
                var child = collisionLayer.gameObject.transform.GetChild(i);

                // Debug.Log("player: "+ this.gameObject.transform.position +", child: "+ child.position);

                var script = child.GetComponent<TileProperties>();
                if( script == null ) 
                    continue;

                var layerName = script.layerName;
                var attributedLayer = getChildGameObject(collisionLevel, layerName);
                // Debug.Log("ABOVE "+ layerName);
                if( attributedLayer != null ) {
                    bool inFrontOfPlayer;

                    // Check player y position
                    if( gameObject.transform.position.y > child.position.y + 14 )
                        inFrontOfPlayer = true; 
                    else 
                        inFrontOfPlayer = false; 

                    // Now, get all available meshes within the level gameObject
                    foreach( string tileMapName in tileMaps ) {
                        // Get the child tilemap 
                        GameObject mesh = getChildGameObject(attributedLayer, tileMapName);

                        if( mesh != null ) {
                            var renderer = mesh.gameObject.GetComponent<Renderer>();
                            if( inFrontOfPlayer )
                                renderer.sortingOrder = 11;
                            else 
                                renderer.sortingOrder = 9;
                        }
                    }
                }
            }
        }

        // if( Input.GetKeyDown(KeyCode.Space) ) {
        //     animator.SetTrigger(jumpHash);
        // }
    }

    void Flip() {
        facingLeft = !facingLeft;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
