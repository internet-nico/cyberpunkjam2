﻿using UnityEngine;

public class SnapToGrid : MonoBehaviour {

    public float offsetX = 0;
    public float offsetY = 0;
    
    void LateUpdate() {
        var newX = gameObject.transform.position.x - (gameObject.transform.position.x % 4) - offsetX;
        var newY = gameObject.transform.position.y - (gameObject.transform.position.y % 4) - offsetY;
        var newPosition = new Vector3((float)newX, (float)newY);

        // Debug.Log("new: ("+ newX +", "+ newY +")");

        gameObject.transform.position = newPosition;
    }
}
