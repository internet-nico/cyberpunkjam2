﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemController : MonoBehaviour {

    public ParticleSystem particleSystem;
    ParticleSystem m_System;
    ParticleSystem.Particle[] m_Particles;

	// Use this for initialization
	void Start () {
	   particleSystem = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        // particleSystem.startSize = transform.lossyScale.magnitude;
	}

    void LateUpdate() {
        InitializeIfNeeded();

        // GetParticles is allocation free because we reuse the m_Particles buffer between updates
        int numParticlesAlive = m_System.GetParticles(m_Particles);

        // Debug.Log(numParticlesAlive +" particles alive");

        // Change only the particles that are alive
        for (int i = 0; i < numParticlesAlive; i++)
        {
            float endX = m_Particles[i].position.x;
            // endX += (m_Particles[i].position.x % 4); 
            float endY = m_Particles[i].position.y;
            // endY += (m_Particles[i].position.y % 4); 
            // float randomX = Random.Range(-50, 10);
            // randomX -= (randomX % 4);
            // float randomY = Random.Range(-50, 10);
            // randomY -= (randomY % 4);

            var newPosition = new Vector3((float)endX, (float)endY);
            // var newPosition = new Vector3((float)randomX, (float)randomY);

            // float life = m_Particles[i].lifetime/m_Particles[i].startLifetime;

            // Debug.Log("particle from ("+ m_Particles[i].position.x +", "+ m_Particles[i].position.y +") to ("+ (float)endX +", "+ (float)endY +")");
            

            // m_Particles[i].velocity += Vector3.left * 20;
            m_Particles[i].position = newPosition;
            
            // Vector3 newP = Vector3.Lerp(m_Particles[i].position, new Vector3(10,50), life);
            // m_Particles[i].position = newP;
        }

        // Apply the particle changes to the particle system
        m_System.SetParticles(m_Particles, numParticlesAlive);
    }

    void InitializeIfNeeded()
    {
        if (m_System == null)
            m_System = GetComponent<ParticleSystem>();

        if (m_Particles == null || m_Particles.Length < m_System.maxParticles)
            m_Particles = new ParticleSystem.Particle[m_System.particleCount]; 
    }
}
