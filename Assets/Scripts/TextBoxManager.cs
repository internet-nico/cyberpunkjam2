﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextBoxManager : MonoBehaviour {

    private const float TYPE_SPEED = 0.025f;

    public PlayerController player;
    public GameObject textBox;
    public Text textLabel;
    public GameObject optionArrow;
    public GameObject yesNo;

    public TextAsset textFile;
    public string[] textLines;
    public bool isActive = true;
    public bool stopPlayerMovement = true;
    public int currentLineIndex;
    public int endAtLineIndex;

    private bool isTextAnimating = false;
    private bool skipTextAnimation = false;
    private bool interactiveBoolean = false;
    private bool enableInteractiveKeys = false;
    private int waitFramesAfterEnabled = 0;
    private int currentInteractiveOption = 0;

	// Use this for initialization
	void Start () {
        // player = FindObjectOfType<PlayerController>();

        if( textFile != null ) {
            textLines = textFile.text.Split("\n".ToCharArray());
        }
	}
	
	// Update is called once per frame
	void Update () {
        if( ! isActive )
            return;

		if (! enableInteractiveKeys && waitFramesAfterEnabled == 0 && Input.GetKeyDown (KeyCode.Space)) {
			// Press to start text animation
			if (isTextAnimating) {
				// Skip animating text to end
				skipTextAnimation = true;
				//currentLineIndex++;
			} else {
				if (currentLineIndex <= endAtLineIndex) {
					Debug.Log ("Animate text: " + textLines [currentLineIndex]);
                    
					// Start coroutine to animate current line of text
					StartCoroutine (AnimateTextLine (textLines [currentLineIndex]));
					currentLineIndex++;
				} else {
					// No more lines to read!
					DisableTextBox ();
				}
			}
		} else if (! enableInteractiveKeys && waitFramesAfterEnabled > 0) {
			// Weird bug where this Input was being triggered on the same frame as the 
			// first Dialog controller and immediately skipping the animation...
			waitFramesAfterEnabled--;
		}

		if (enableInteractiveKeys) {
			if (Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)) {
				currentInteractiveOption--;

				optionArrow.transform.localPosition = new Vector3(optionArrow.transform.localPosition.x, 0.0f, optionArrow.transform.localPosition.y);

                if (currentInteractiveOption < 0)
                    currentInteractiveOption = 0;
            }

            if (Input.GetKeyDown (KeyCode.DownArrow) || Input.GetKeyDown (KeyCode.S)) {
                currentInteractiveOption--;

				optionArrow.transform.localPosition = new Vector3(optionArrow.transform.localPosition.x, -21.5f, optionArrow.transform.localPosition.y);

				if (currentInteractiveOption > 1)
					currentInteractiveOption = 1;
			}

			if (Input.GetKeyDown (KeyCode.KeypadEnter) || Input.GetKeyDown (KeyCode.Space)) {
				// Dispatch option selected event
				if (currentInteractiveOption == 0) {
					EventManager.TriggerEvent ("interactiveOptionSelectedNo");
				} else {
					EventManager.TriggerEvent ("interactiveOptionSelectedYes");
				}

				// Stop interactive mode
				enableInteractiveKeys = false;

				// Close dialog box
				DisableTextBox ();
			}
		}
			
    }

    private IEnumerator AnimateTextLine(string lineOfText) {
        // Reset
        int letter = 0;
        textLabel.text = "";
        isTextAnimating = true;
        skipTextAnimation = false;

        while( isTextAnimating && ! skipTextAnimation && (letter < lineOfText.Length - 1) ) {
            textLabel.text += lineOfText[letter];

            // Increment next letter
            letter += 1;

            // Wait to loop again
            yield return new WaitForSeconds(TYPE_SPEED);
        }

        // Show completed text line
        textLabel.text = lineOfText;

		// Display a yes/no question
		if (interactiveBoolean) { 
            enableInteractiveKeys = true;

            // Reset options
            currentInteractiveOption = 0;
            optionArrow.transform.localPosition = new Vector3(optionArrow.transform.localPosition.x, 0.0f, optionArrow.transform.localPosition.y);
            
            // Enable options
            yesNo.SetActive(true);
            optionArrow.SetActive(true);
		}

        // Completed
        isTextAnimating = false;
        skipTextAnimation = false;
    }

    public void EnableTextBox() {
        textBox.SetActive(true);
        isActive = true;
		waitFramesAfterEnabled = 5;

        // Remove interactive elements
        optionArrow.SetActive(false);
        yesNo.SetActive(false);

        if( stopPlayerMovement )
            player.canMove = false;

        StartCoroutine(AnimateTextLine(textLines[currentLineIndex]));
		currentLineIndex++;
    }

    public void DisableTextBox() {
        textBox.SetActive(false);
        isActive = false;
        player.canMove = true;

        // Dispatch completed event
        EventManager.TriggerEvent("dialogComplete");
    }

	public void ReloadTextAtLine(TextAsset newText, int startAtIndex, int endAtIndex = 0, bool interactive = false) {
        if( newText != null ) {
            textLines = newText.text.Split("\n".ToCharArray());

            currentLineIndex = startAtIndex;
            endAtLineIndex = endAtIndex; // == 0 ? textLines.Length - 1 : endAtIndex;
			interactiveBoolean = interactive;

            EnableTextBox();
        }
    }
}
