﻿using UnityEngine;
using System.Collections;

public class ProfDialogController : MonoBehaviour {

	private const int COOLDOWN = 10;

    public TextAsset textAsset;
    public TextBoxManager textBoxManager;

    private bool waitingForPress = false;
    private bool dialogInProgress = false;
    private int[,] textOrder = new int[,] { {0,0}, {1,2}, {3,4}, {5,5}, {6,6}, {7,7}, {8,8}, {9,9} };
    private int textIndex = 0;
    private int dialogCooldown = 0;

	// Use this for initialization
	void Start () {
        textBoxManager = FindObjectOfType<TextBoxManager>();	
	}

	// Update is called once per frame
	void Update () {
		if (dialogInProgress)
			return;
		
        // Check dialog cooldown
        if( dialogCooldown == 0 ) {
            // Check if button needs to be pressed
			if( waitingForPress && Input.GetKeyDown(KeyCode.Space) ) {
                dialogInProgress = true;

				// Repeat the last index over and over
				if( textIndex > textOrder.GetLength(0) - 1 )
					textIndex = textOrder.GetLength(0) - 1;
				
				// Debug.Log("Prof Dialog: "+ textOrder[textIndex, 0] +"-"+ textOrder[textIndex, 1]);

                textBoxManager.ReloadTextAtLine(textAsset, textOrder[textIndex, 0], textOrder[textIndex, 1]);
                
                textIndex++;

                // Start listening for complete event
                EventManager.StartListening ("dialogComplete", DialogCompletedHandler);
            }
        }
        else {
            // Cool down
            dialogCooldown--;
        }
	}

    void DialogCompletedHandler() {
        // Stop listening
        EventManager.StopListening ("dialogComplete", DialogCompletedHandler);

        // Wait number of frames before dialog can happen again
		dialogInProgress = false;
		dialogCooldown = COOLDOWN;
    }

    void OnTriggerEnter2D(Collider2D other) {
        // Debug.Log("Prof triggered by "+ other.name);

        if( other.name == "Player" ) {
			waitingForPress = true;
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        // Debug.Log("Prof exit");

        if( other.name == "Player" ) {
            waitingForPress = false;
            dialogInProgress = false;
        }
    }
}
